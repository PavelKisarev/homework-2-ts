import { MOVIE_API_KEY } from '../constants/constants'
import HTTPRequest from '../http/http-request'
import moviesListRender from "./moviesListRender"

async function popularFilmRender(): Promise<void> {

    const popularFilms = await HTTPRequest(`https://api.themoviedb.org/3/movie/popular?api_key=${MOVIE_API_KEY}`)

    const moviesContainer = document.getElementById('film-container')

    if (moviesContainer) {
        moviesContainer.innerHTML = ''
    }
    if (popularFilms != undefined) {
        moviesListRender(popularFilms)
    }
    else { console.warn("ERROR") }


}

export default popularFilmRender
import { MOVIE_API_KEY } from '../constants/constants'
import HTTPRequest from '../http/http-request'


async function randomFilmRender(): Promise<void> {
    const popularFilms = await HTTPRequest(`https://api.themoviedb.org/3/movie/popular?api_key=${MOVIE_API_KEY}`)

    if (popularFilms) {
        const randomFilm = popularFilms[Math.floor(Math.random() * popularFilms.length)]
        const overviewText = document.getElementById('random-movie-description')
        const overviewTitle = document.getElementById('random-movie-name')

        if (overviewTitle === null) {
            console.warn('ERORR')
            return
        }
        if (overviewText === null) {
            console.warn('ERORR')
            return
        }

        overviewTitle.innerHTML = `${randomFilm.title}`
        overviewText.innerHTML = `${randomFilm.overview}`

        const randMovieBlock = <HTMLElement>overviewTitle.closest("#random-movie")

        if (randMovieBlock) {
            randMovieBlock.style.backgroundImage = `url(https://image.tmdb.org/t/p/original${randomFilm.backdrop_path})`
        }
    }

}

export default randomFilmRender
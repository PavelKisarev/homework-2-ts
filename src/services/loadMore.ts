import { MOVIE_API_KEY } from '../constants/constants'
import HTTPRequest from '../http/http-request'
import moviesListRender from "./moviesListRender"

async function loadMore(pageNum: number): Promise<void> {

    const popularFilms = await HTTPRequest(`https://api.themoviedb.org/3/movie/popular?api_key=${MOVIE_API_KEY}&page=${pageNum}`)

    if (popularFilms) {
        moviesListRender(popularFilms)
    }

}

export default loadMore
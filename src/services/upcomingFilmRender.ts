import { MOVIE_API_KEY } from '../constants/constants'
import HTTPRequest from '../http/http-request'
import moviesListRender from "./moviesListRender"

async function upcomingFilmRender(): Promise<void> {

    const upcomingFilms = await HTTPRequest(`https://api.themoviedb.org/3/movie/upcoming?api_key=${MOVIE_API_KEY}`)

    if (upcomingFilms) {
        const moviesContainer = document.getElementById('film-container')
        if (moviesContainer) {
            moviesContainer.innerHTML = ''
        }

        moviesListRender(upcomingFilms)
    }

}

export default upcomingFilmRender
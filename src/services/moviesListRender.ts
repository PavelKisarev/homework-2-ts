import createElem from "../helpers/element.creator"
import IMovie from "../interfaces/movie.interface"

function moviesListRender(moviesList: IMovie[]): void {

    const moviesContainer = document.getElementById('film-container')

    for (const movie of moviesList) {

        const div = createElem('div', ['col-lg-3', 'col-md-4', 'col-12', 'p-2'])

        const card = createElem('div', ['card', 'shadow-sm'])

        const img = createElem('img')
        img.setAttribute('src', `https://image.tmdb.org/t/p/original${movie.poster_path}`)

        const cardBody = createElem('div', ['card-body'])

        const cardText = createElem('p', ['card-text', 'truncate'])
        cardText.innerHTML = `${movie.overview}`

        const dateContainer = createElem('div', ['d-flex', 'justify-content-between', 'align-items-center'])
        const smallDate = createElem('span', ['text-muted'])
        smallDate.innerHTML = `${movie.release_date}`

        dateContainer.append(smallDate)

        cardBody.append(cardText, dateContainer)

        card.append(img, cardBody)

        div.append(card)

        moviesContainer?.append(div)
    }


}


export default moviesListRender
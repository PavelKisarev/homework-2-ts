import { MOVIE_API_KEY } from '../constants/constants'
import HTTPRequest from '../http/http-request'
import moviesListRender from "./moviesListRender"

async function topRatedFilmRender(): Promise<void> {

    const topRatedFilms = await HTTPRequest(`https://api.themoviedb.org/3/movie/top_rated?api_key=${MOVIE_API_KEY}`)

    if (topRatedFilms) {
        const moviesContainer = document.getElementById('film-container')

        if (moviesContainer) {
            moviesContainer.innerHTML = ''
        }
        moviesListRender(topRatedFilms)
    }


}

export default topRatedFilmRender
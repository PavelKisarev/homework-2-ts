import { MOVIE_API_KEY } from '../constants/constants'
import HTTPRequest from '../http/http-request'
import moviesListRender from "./moviesListRender"


async function searchFilm(searchString: string): Promise<void> {

    const searchedFilms = await HTTPRequest(`https://api.themoviedb.org/3/search/movie?api_key=${MOVIE_API_KEY}&query=${searchString}`)

    if (searchedFilms) {
        const moviesContainer = document.getElementById('film-container')

        if (moviesContainer) {
            moviesContainer.innerHTML = ''
        }

        moviesListRender(searchedFilms)
    }

}

export default searchFilm
function createElem(tag: string, classes: Array = []) {
    const elem = document.createElement(tag)
    elem.classList.add(...classes)

    return elem
}

export default createElem
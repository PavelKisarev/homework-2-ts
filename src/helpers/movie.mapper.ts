import IMovie from "../interfaces/movie.interface";


function movieMapper(curMovieArr: []): IMovie[] {

    const movies = curMovieArr.map((el) => {
        const { id, title, overview, poster_path, backdrop_path, release_date } = el
        return { id, title, overview, poster_path, backdrop_path, release_date }
    })

    return movies
}

export default movieMapper
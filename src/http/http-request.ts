import movieMapper from "../helpers/movie.mapper"
import IMovie from "../interfaces/movie.interface"

async function HTTPRequest(url: string): Promise<IMovie[] | undefined> {

    try {
        const data = await fetch(url)

        const respose = await data.json()
        const mappedResult = movieMapper(respose.results)

        return mappedResult
    } catch (error) {
        console.warn(error.message)
        return undefined
    }
}

export default HTTPRequest
import randomFilmRender from "./services/randomFilmRender";
import upcomingFilmRender from "./services/upcomingFilmRender";
import topRatedFilmRender from "./services/topRatedFilmRender";
import popularFilmRender from "./services/popularFilmRender";
import loadMore from "./services/loadMore";
import searchFilm from "./services/searchFilm";


export async function render(): Promise<void> {
    // TODO render your app here

    randomFilmRender()
    popularFilmRender()

    const popularBtn = document.getElementById('popular')
    popularBtn?.addEventListener('click', (): void => {
        popularFilmRender()
    })

    const upcomingBtn = document.getElementById('upcoming')
    upcomingBtn?.addEventListener('click', (): void => {
        upcomingFilmRender()
    })

    const topRatedBtn = document.getElementById('top_rated')
    topRatedBtn?.addEventListener('click', (): void => {
        topRatedFilmRender()
    })

    const loadMoreBtn = document.getElementById('load-more')
    let pageIndex = 1
    loadMoreBtn?.addEventListener('click', (): void => {
        pageIndex++
        loadMore(pageIndex)
    })

    const searchBtn = document.getElementById('submit')
    const searchInput = <HTMLInputElement>document.getElementById('search')
    searchBtn?.addEventListener('click', (): void => {
        const searchString = searchInput?.value
        if (searchString !== '') {
            searchFilm(searchString)
            searchInput.value = ''
        }
    })

}
